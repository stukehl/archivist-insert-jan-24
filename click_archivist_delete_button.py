#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to delete everything
"""

import sys
import time

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions


options = FirefoxOptions()
driver = webdriver.Remote(
        'http://selenium-standalone-firefox:4444/wd/hub',
        options=options)


def archivist_login(url, uname, pw):
    """
    Log in to archivist
    """
    driver.get(url)
    time.sleep(5)
    driver.find_element(By.ID, "email").send_keys(uname)
    driver.find_element(By.ID, "password").send_keys(pw)
    driver.find_element(By.XPATH, '//span[text()="Log In"]').click()
    time.sleep(5)


def click_delete_button(uname, pw):
    """
    Click 'Delete' for the only study
    """
    # log in
    archivist_url = "https://closer-build.herokuapp.com"
    export_url = "https://closer-build.herokuapp.com/admin/instruments/"
    archivist_login(archivist_url, uname, pw)
    driver.get(export_url)
    time.sleep(10)

    # locate id and link
    trs = driver.find_elements(By.XPATH, "html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")

    for i in range(0, len(trs)):
        tr = trs[i]

        # column 2 is "Prefix"
        prefix = tr.find_elements(By.XPATH, "td")[1].text
        print(prefix)

        # column 4 is "Actions", click on "Delete"
        deleteButton = tr.find_elements(By.XPATH, "td")[3].find_elements(By.XPATH, "div/button")[-1]

        print("Click delete button for " + prefix)
        deleteButton.click()

        time.sleep(5)
        driver.find_element(By.ID, "outlined-error-helper-text").send_keys(prefix)
        time.sleep(5)
        driver.find_element(By.XPATH, '//span[text()="Delete"]').click()

    driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    click_delete_button(uname, pw)


if __name__ == "__main__":
    main()
