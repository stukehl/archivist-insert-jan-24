from pytest import raises
import pandas as pd
import os
from pathlib import Path


# run "pytest" on command line (maybe "pytest-3" on older computers)

# template
template_dir = Path('template')
template_files = [template_dir / f for f in os.listdir(template_dir) if os.path.isfile(os.path.join(template_dir, f))]

# list of template file names
valid_names = ['question_item', 'codelist', 'loop', 'condition', 'sequence', 'statement', 'response', 'question_grid']

tables_dir = Path('archivist_tables_utf8')
allfiles = [tables_dir / f for f in os.listdir(tables_dir) if (tables_dir / f).is_file() and (tables_dir / f).name.lower() != 'readme.txt']


def test_same_type():
    """
    All input files should have the same suffix.
    """
    t = [f.suffix for f in allfiles]
    if not len(set(t)) == 1:
        print(set(t))
    assert len(set(t)) == 1


def test_names_subset():
    """
    All input files should have valid names
    """
    names = [f.stem for f in allfiles]
    for name in names:
        if not name in valid_names:
            print(name)
        assert name in valid_names


def test_headers_match_template():
    """
    All input files should have valid headers
    """
    for f in allfiles:
        g = template_dir / f.with_suffix('.csv').name
        df_input = pd.read_csv(f, sep=None, engine='python')
        df_template = pd.read_csv(g, sep=None, engine='python')
        if not df_input.columns.to_list() == df_template.columns.to_list():
            print(f)
            print(df_input.columns.to_list())
        assert df_input.columns.to_list() == df_template.columns.to_list()


def test_unique_label():
    """
    All input files (except codelist) should not have duplicated lables
    """
    for f in allfiles:
        if not f.stem == 'codelist':

            df_input = pd.read_csv(f, sep=None, engine='python')
            if f.stem == 'question_item':
                df = df_input.loc[(df_input.rd_order == 1), :]
            else:
                df = df_input

            if not df[df['Label'].duplicated()].empty:
                print(f)
                print(df[df['Label'].duplicated()])

            assert df['Label'].nunique() == len(df['Label'])


def test_special_characters():
    """
    All Lable columns should not have non-standard characters
    """
    for f in allfiles:
        df_input = pd.read_csv(f, sep=None, engine='python')
        for item in ["(", ")", " "]:
            if not item in df_input['Label']:
                print(item)
            assert item not in df_input['Label']


def test_code_response():
    """
    Same codelist should have same min/max response
    """

    # read question grid file
    qg_file = tables_dir / 'question_grid.csv'
    if Path.exists(qg_file):
        df_qg = pd.read_csv(qg_file, sep=None, engine='python')
    else:
        df_qg = pd.DataFrame(columns = ['Horizontal_Codelist_Name', 'Horizontal_min_responses', 'Horizontal_max_responses',
                                        'Vertical_Codelist_Name', 'Vertical_min_responses', 'Vertical_max_responses'])

    # read question item file
    qi_file = tables_dir / 'question_item.csv'
    if Path.exists(qi_file):
        df_qi = pd.read_csv(qi_file, sep=None, engine='python')
    else:
        df_qi = pd.DataFrame(columns = ['Response', 'min_responses', 'max_responses'])

    # get min/max response from question item file
    df_qi_response = df_qi.loc[(df_qi.Response != None), ['Response', 'min_responses', 'max_responses']]
    df_qi_response.rename(columns={'Response': 'Label'}, inplace=True)

    # get min/max response from question grid file
    df_qg_horizontal = df_qg.loc[(df_qg.Horizontal_Codelist_Name != None), ['Horizontal_Codelist_Name', 'Horizontal_min_responses', 'Horizontal_max_responses']]
    df_qg_horizontal.rename(columns={'Horizontal_Codelist_Name': 'Label',
                                     'Horizontal_min_responses': 'min_responses',
                                     'Horizontal_max_responses': 'max_responses'}, inplace=True)

    df_qg_vertical = df_qg.loc[(df_qg.Vertical_Codelist_Name != None), ['Vertical_Codelist_Name', 'Vertical_min_responses', 'Vertical_max_responses']]
    df_qg_vertical.rename(columns={'Vertical_Codelist_Name': 'Label',
                                   'Vertical_min_responses': 'min_responses',
                                   'Vertical_max_responses': 'max_responses'}, inplace=True)
    df_gq_response = pd.concat([df_qg_horizontal, df_qg_vertical])

    # combine all response from qi and qg
    df_qi_qg_response_all = pd.concat([df_qi_response, df_gq_response])
    df_qi_qg_response= df_qi_qg_response_all.drop_duplicates(keep='first').reset_index()
    # codelist only
    df_qi_qg_codelist = df_qi_qg_response.loc[df_qi_qg_response['Label'].str.startswith('cs_', na=False)] 

    # duplicated row
    df_dup = df_qi_qg_codelist[df_qi_qg_codelist.duplicated('Label')]

    if not df_dup.empty:
        print(df_dup)
    assert df_dup.empty, 'same code list needs to have same min/max'


def test_qi_response():
    """
    All question items should contain response domain
    """

    # read question item file
    qi_file = tables_dir / 'question_item.csv'
    if Path.exists(qi_file):
        df_qi = pd.read_csv(qi_file, sep=None, engine='python')
    else:
        df_qi = pd.DataFrame(columns = ['Response', 'min_responses', 'max_responses'])

    if df_qi['Response'].isnull().values.any():
        print(df_qi[df_qi['Response'].isnull()])
    assert not df_qi['Response'].isnull().values.any(), 'question item needs to have response domain'


def test_question_literal():
    """
    All questions should contain literal
    """

    # read question item file
    qi_file = tables_dir / 'question_item.csv'
    if Path.exists(qi_file):
        df_qi = pd.read_csv(qi_file, sep=None, engine='python')[['Label', 'Literal', 'Instructions']]
    else:
        df_qi = pd.DataFrame(columns = ['Label', 'Literal', 'Instructions'])

    # read question grid file
    qg_file = tables_dir / 'question_grid.csv'
    if Path.exists(qg_file):
        df_qg = pd.read_csv(qg_file, sep=None, engine='python')[['Label', 'Literal', 'Instructions']]
    else:
        df_qg = pd.DataFrame(columns = ['Label', 'Literal', 'Instructions'])

    df = pd.concat([df_qi, df_qg])
    if df['Literal'].isnull().values.any():
        print(df[df['Literal'].isnull()])

    assert not df['Literal'].isnull().values.any(), 'question should have literal'


def test_statement_literal():
    """
    All statements should contain literal
    """

    # read statement file
    statement_file = tables_dir / 'statement.csv'
    if Path.exists(statement_file):
        df_statement = pd.read_csv(statement_file, sep=None, engine='python')
    else:
        df_statement = pd.DataFrame(columns = ['Label', 'Literal', 'Parent_Type', 'Parent_Name', 'Branch;Position'])

    if df_statement['Literal'].isnull().values.any():
        print(df_statement[df_statement['Literal'].isnull()])

    assert not df_statement['Literal'].isnull().values.any(), 'statement should have literal'


def test_parent_label():
    """
    All child should have parent lable
    """
    num_empty = 0
    for f in allfiles:
        if not f.stem in ('codelist', 'response'):

            df_input = pd.read_csv(f, sep=None, engine='python')
            if f.stem == 'question_item':
                df = df_input.loc[(df_input.rd_order == 1), :]
            else:
                df = df_input
            n = df['Parent_Name'].isnull().values.sum()

            num_empty = num_empty + n

            if not df[df['Parent_Name'].isnull()].empty:
                print(f)
                print(df[df['Parent_Name'].isnull()][['Label', 'Parent_Type', 'Parent_Name']])

    assert num_empty == 1, 'child should have a parent name'


def test_parent_type():
    """
    All child should have parent type
    """
    num_empty = 0
    for f in allfiles:
        if not f.stem in ('codelist', 'response'):

            df_input = pd.read_csv(f, sep=None, engine='python')
            if f.stem == 'question_item':
                df = df_input.loc[(df_input.rd_order == 1), :]
            else:
                df = df_input
            n = df['Parent_Type'].isnull().values.sum()

            num_empty = num_empty + n

            if not df[df['Parent_Type'].isnull()].empty:
                print(f)
                print(df[df['Parent_Type'].isnull()][['Label', 'Parent_Type', 'Parent_Name']])

    assert num_empty == 0, 'child should have a parent type'


def test_same_question_same_position():
    """
    Same question should have same position
    """

    # read question item file
    qi_file = tables_dir / 'question_item.csv'
    if Path.exists(qi_file):
        df_qi = pd.read_csv(qi_file, sep=None, engine='python')
    else:
        df_qi = pd.DataFrame(columns = ['Response', 'min_responses', 'max_responses'])

    # question with multiple rows
    df = df_qi[df_qi[['Label']].duplicated()]
    # question with same position
    df1 = df_qi[df_qi[['Label', 'Position']].duplicated()]
    if not df.empty:
        if not df.shape == df1.shape:
            print(df_qi[df_qi['Label'].isin(df['Label'])][['Label', 'Position']])
        assert df.shape == df1.shape, 'same question item needs to have same position number'

